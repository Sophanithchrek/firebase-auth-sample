document.querySelector('#btn-register').onclick = async () =>{

    let fullName = document.getElementById(`full-name`).value

    let email =  document.getElementById(`email`).value

    let empCode = document.getElementById(`emp-code`).value

    let requestObject = {
        token : ""
    }

    requestObject['fullName'] = fullName
    requestObject['email'] = email
    requestObject['code'] = empCode

    if(fullName==""){
        setMessage("#full-name-error-msg")
        return
    } else if (email == ""){
        setMessageSelect("#email-error-msg")
        return
    } else if (empCode == ""){
        setMessageSelect("#emp-code-error-msg")
        return
    } else {
        fetch('http://localhost:8080/api/v1/user/register', {
            method: 'POST', // or 'PUT'
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestObject),
        }).then(response => {
                console.log("User is has been registered successfully")
                window.location.href = '/dashboard'
            }).catch(err => {
            alert(`This error while sign-up information:::: ${err}`)
        })
    }

}