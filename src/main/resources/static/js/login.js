const auth = firebase.auth()
const TOKEN_KEY = "my_token"

function requestLogin() {

    var email = document.getElementById('email_field').value
    var pass = document.getElementById('password_field').value

    const promise = auth.signInWithEmailAndPassword(email, pass)
    promise.then(res => {
        let {za} = res.user
        setCookie(TOKEN_KEY,za,0.2)
        console.log(za," >>> token")
        window.location.href=`/check-token/${za}`
    })
    promise.catch(e => alert(e.message))

}

function requestLoginWithGoogle() {
    window.sessionStorage.setItem('pending', 1);
    let provider = new firebase.auth.GoogleAuthProvider()
    // firebase.auth().signInWithRedirect(provider)
    firebase.auth().signInWithPopup(provider).then(function (result) {
        let {za} = result.user
        setCookie(TOKEN_KEY,za,0.2)
        console.log(za," >>> token")
        window.location.href=`/check-token/${za}`
    }).catch(function (err) {
        console.log(err)
    })
}

const setCookie = (name,value,days) => {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(cookieName) {
    let cookie = {};
    document.cookie.split(';').forEach(function(el) {
        let [key,value] = el.split('=');
        cookie[key.trim()] = value;
    })
    return cookie[cookieName];
}
