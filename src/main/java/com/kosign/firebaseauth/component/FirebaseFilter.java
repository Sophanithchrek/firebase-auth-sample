package com.kosign.firebaseauth.component;

import com.kosign.firebaseauth.model.user.User;
import com.kosign.firebaseauth.payload.FirebaseTokenHolder;
import com.kosign.firebaseauth.service.FirebaseService;
import com.kosign.firebaseauth.service.UserService;
import com.kosign.firebaseauth.util.AuthSession;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class FirebaseFilter extends OncePerRequestFilter {

	private static String HEADER_NAME = "X-Authorization-Firebase";

	@Autowired
	private FirebaseService firebaseService;

	@Autowired
	private UserService userService;

	@Autowired
	AuthSession authSession;

//	public FirebaseFilter(FirebaseService firebaseService) {
//		this.firebaseService = firebaseService;
//	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		String xAuth = request.getHeader(HEADER_NAME);
		if(xAuth == null && ObjectUtils.anyNotNull(authSession.getMyToken())) xAuth = authSession.getMyToken();

		if (StringUtils.isBlank(xAuth)) {

			filterChain.doFilter(request, response);
			return;
		} else {
			try {

				FirebaseTokenHolder holder = firebaseService.parseToken(xAuth);

				String userId = holder.getUid();

			    User credUser = userService.findById(userId);

				Authentication auth = new FirebaseAuthenticationToken(credUser.getUsername(), holder,credUser.getAuthorities());
				SecurityContextHolder.getContext().setAuthentication(auth);

				filterChain.doFilter(request, response);
			}catch (Exception e){
				filterChain.doFilter(request, response);
				return;
			}
		}
	}

}
