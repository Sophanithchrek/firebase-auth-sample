/**
 * File Name        	: BusinessException.java
 * File Path        	: bnk-core-api/src/main/java/com/bnkcmfi/api/exception/BusinessException.java
 * File Description 	: BusinessException
 * File Author	  		:
 * Created Date	  	    : 05-August-2020 09:00 AM
 * Developed By	  	    :
 * Modified Date	  	:
 * Modified By          :
 **/
package com.kosign.firebaseauth.exception;

import com.kosign.firebaseauth.enums.ErrorCode;

/**
 * Handle exception for Business Exception
 */
public class BusinessException extends RuntimeException {

    private ErrorCode errorCode;

    public BusinessException(ErrorCode errorCode, String message) {

        super(message);
        this.errorCode = errorCode;

    }

    public BusinessException(ErrorCode errorCode) {

        super(errorCode.getMessage());
        this.errorCode = errorCode;

    }

    public BusinessException(ErrorCode errorCode, Throwable e) {

        this(errorCode);
        System.out.println(e);
//        AppLogManager.error(e);

    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
