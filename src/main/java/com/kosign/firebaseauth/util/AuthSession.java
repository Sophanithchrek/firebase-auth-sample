package com.kosign.firebaseauth.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@Component
//@Scope(value  = WebApplicationContext.SCOPE_SESSION  , proxyMode = ScopedProxyMode.TARGET_CLASS)
@Getter
@Setter
public class AuthSession {

    private String myToken;

    private String sessionId;

    private static final String COOKIE_TOKEN = "my_token";

    @Autowired
    HttpServletRequest request;

    @Autowired
    HttpServletResponse response;

    public String getMyToken() {
        if(ObjectUtils.allNull(request.getCookies()))
            return null;
        Optional<Cookie> cookieToken = Arrays.stream(request.getCookies()).filter(cookie -> COOKIE_TOKEN.equalsIgnoreCase(cookie.getName())).findAny();
        return cookieToken.isPresent()? cookieToken.get().getValue() : null;
    }

    public void clearToken() {
        this.myToken = null;
    }

    public void setCookieToken(String token){
        CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        cookieLocaleResolver.setCookieName(COOKIE_TOKEN);
        cookieLocaleResolver.addCookie(response,token);

    }

}
