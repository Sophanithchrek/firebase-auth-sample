package com.kosign.firebaseauth.util;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.kosign.firebaseauth.payload.FirebaseTokenHolder;
import org.apache.commons.lang3.StringUtils;

public class FirebaseParser {
    public FirebaseTokenHolder parseToken(String idToken) {
        if (StringUtils.isBlank(idToken)) {
            throw new IllegalArgumentException("FirebaseTokenBlank");
        }

        try {
            FirebaseToken authTask = FirebaseAuth.getInstance().verifyIdToken(idToken);
            return new FirebaseTokenHolder(authTask);
        } catch (FirebaseAuthException  e) {
            System.out.println("**********************Firebase Error ***************************");
            System.out.println("**********************Firebase Error End***************************");
            return null;
        }
    }
}
