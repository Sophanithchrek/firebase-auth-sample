package com.kosign.firebaseauth.controller;

import com.kosign.firebaseauth.payload.RequestRegisterUserInfo;
import com.kosign.firebaseauth.payload.UserResponse;
import com.kosign.firebaseauth.service.FirebaseService;
import com.kosign.firebaseauth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    FirebaseService firebaseService;

    @Autowired
    UserService userService;

    @PostMapping("api/v1/user/register")
    public void register(@RequestBody RequestRegisterUserInfo info) {
        userService.register(info);
    }

    @GetMapping("/api/v1/user/profile")
    public UserResponse getProfile() {
        return userService.getUserProfile();
    }

}
