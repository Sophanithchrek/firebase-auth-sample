package com.kosign.firebaseauth.controller;

import com.kosign.firebaseauth.config.IAuthenticationFacade;
import com.kosign.firebaseauth.model.user.User;
import com.kosign.firebaseauth.service.FirebaseService;
import com.kosign.firebaseauth.service.UserService;
import com.kosign.firebaseauth.util.AuthSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Controller
public class WebController {

    @Autowired
    FirebaseService firebaseService;

    @Autowired
    AuthSession authSession;

    @Autowired
    UserService userService;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    @Autowired
    HttpServletRequest request;

    @Autowired
    HttpServletResponse response;

    @Value("${ksg.base-url}")
    private String baseUrl;

    @GetMapping(value = {"/sign-up","signup"})
    public String signUp(){
        return "signup";
    }

    @GetMapping("/check-token/{token}")
    public String checkToken(@PathVariable("token") String token ){

        Optional<User> optionalUser = userService.checkTokenToTakeUserInfo(token);
        authSession.setMyToken(token);
        System.err.println("My user data is: "+ optionalUser);

        if (!optionalUser.isPresent()) {
            return "redirect:/sign-up-information";
        } else {
            return "redirect:/dashboard";
        }

    }

    @GetMapping("/sign-up-information")
    public String signUpInformation(){
        return "profile";
    }

    @GetMapping(value = {"/sign-in","/"})
    public String getLogin () {
        return "login";
    }

    @GetMapping("/dashboard")
    public String getDashboard() {
        return "/dashboard";
    }

    @GetMapping("/sign-out")
    public String redirectToLogin () {
//        revoke token here
        authSession.clearToken();
        return "redirect:"+baseUrl+"/sign-in";
    }

    @ResponseBody
    @GetMapping("/my-token")
    public String getToken () {
        return authSession.getMyToken();
    }

}
