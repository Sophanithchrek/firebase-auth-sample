package com.kosign.firebaseauth.enums;

import org.springframework.http.HttpStatus;

public enum ErrorCode {

    // 403 Forbidden
    FORBIDDEN_TRANSACTION(HttpStatus.FORBIDDEN, "Requested transaction is not yours"),

    // 404 Not Found
    EMAIL_NOT_FOUND(HttpStatus.BAD_REQUEST,"Email not found"),
    USER_NOT_FOUND(HttpStatus.NOT_FOUND, "You haven't signed up yet"),
    EMPLOYEE_NOT_FOUND(HttpStatus.NOT_FOUND, "Employee is not found"),
    RESET_TOKEN_NOT_FOUND(HttpStatus.NOT_FOUND,"Reset passcode token not found"),
    MANAGER_NOT_FOUND(HttpStatus.NOT_FOUND,"Manager is not found"),

    //400 Bad Request
    BAD_REQUESTS(HttpStatus.BAD_REQUEST,"Bad request"),
    DATETIME_NOT_ALLOWED(HttpStatus.BAD_REQUEST,"Date time not allow"),
    RESET_TOKEN_NOT_AVAILABLE(HttpStatus.BAD_REQUEST,"Reset passcode token not available"),
    SEND_MAIL_FAILURE(HttpStatus.BAD_REQUEST,"Email does not send success"),
    PIN_NOT_AVAILABLE(HttpStatus.BAD_REQUEST,"Pin code does not available"),
    SESSION_EXPIRED(HttpStatus.BAD_REQUEST,"Session has been expired"),
    TIME_EXIT(HttpStatus.BAD_REQUEST,"Meeting time have already"),
    DATE_EXIT(HttpStatus.BAD_REQUEST,"Meeting date have already"),
    DATE_TIME(HttpStatus.BAD_REQUEST,"This booking is exit"),
    TIME_NOT_CORRECT(HttpStatus.BAD_REQUEST,"Your time not correct"),
    DATE_NOT_CORRECT(HttpStatus.BAD_REQUEST,"Your date not correct"),
    CREATE_ERROR(HttpStatus.BAD_REQUEST, "{0} is already exist in ({1})"),


    PASSCODE_NOT_ACCEPT(HttpStatus.BAD_REQUEST,"Only 6 digits of passcode is allow"),
    PASSCODE_NOT_MATCH(HttpStatus.BAD_REQUEST, "Current passcode is not match"),
    SESSION_ID_NOT_MATCH(HttpStatus.BAD_REQUEST, "Session id is not match."),

    USER_URGENT_NOT_FOUND(HttpStatus.NOT_FOUND, "User agent not found"),
    TWO_STEP_PASSWORD_NOT_ACCEPT(HttpStatus.BAD_REQUEST, "Only 8 to 20 digits of two-step password is allow"),
    CURRENT_PASSWORD_NOT_MATCH(HttpStatus.BAD_REQUEST, "Current password is not match"),

    ;

    private String message;
    private HttpStatus status;

    ErrorCode(final HttpStatus status, final String message) {

        this.message = message;
        this.status = status;
    }

    public String getMessage() {

        return this.message;

    }

    public HttpStatus getStatus() {

        return status;

    }
}
