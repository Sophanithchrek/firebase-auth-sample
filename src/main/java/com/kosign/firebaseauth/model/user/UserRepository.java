package com.kosign.firebaseauth.model.user;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface  UserRepository extends JpaRepository<User, String> {

    Optional<User> findByUsername(@Param("username") String username);

}
