package com.kosign.firebaseauth.model.user;

import com.kosign.firebaseauth.model.UpdatableEntity;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@ToString(exclude = "bookings")
@Entity
@Data
@Table(name="users")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class User extends UpdatableEntity implements UserDetails {

    @Id
    private  String id;

    private String fullName;

    private String identifier;

    private String userCode;

    private String provider;

    private String status;

    private String username;

    private String password;

    private String email;


//    private List<String> roles = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    @ElementCollection(fetch = FetchType.EAGER   )
    @Column(name="role")
    private Set<UserRole> roles;

//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
//
//        roles.forEach(role -> {
//
////            if(role == UserRole.ADMIN || role == UserRole.USER ) {
//            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+role));
////            }
//
//        });
//        return grantedAuthorities;
//    }
    @Override
    public Set<GrantedAuthority> getAuthorities(){

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

        roles.forEach(role -> {
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+role.getValue()));
        });

        return grantedAuthorities;
    }


    @Builder
    public User (String id,String fullName,String identifier,String provider,UserRole role,String userCode,String username,String email){
        this.id=id;
        this.fullName=fullName;
        this.identifier=identifier;
        this.provider=provider;
        this.userCode=userCode;
        roles = new HashSet<>();
        roles.add(role);
        this.username=username;
        this.email = email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }


}
