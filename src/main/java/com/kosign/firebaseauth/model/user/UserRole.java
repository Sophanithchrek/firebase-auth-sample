package com.kosign.firebaseauth.model.user;

import com.fasterxml.jackson.annotation.*;

import java.util.stream.*;

public enum UserRole {

    ADMIN("ADMIN"),
    USER("USER"),
    ;
    private final String value;

    private UserRole(String value) {
        this.value = value;
    }

    /**
     * Method fromValue : Check Enum value
     *
     * @param value  value that have to check
     * @return enum value
     */
    @JsonCreator
    public static UserRole fromValue(String value) {

        return Stream.of(UserRole.values()).filter(targetEnum -> targetEnum.value.equals(value)).findFirst().orElse(null);

    }

    /**
     * Method getValue  : Get Enum value
     * @return Enum value
     */
    @JsonValue
    public String getValue() {
        return value;
    }

    /**
     * Method getLabel  : Get Enum Label
     * @return Enum Label
     */
    public String getLabel() {

        String label = "(no label)";

//        if("M".equals(value)) label = "Male";
//        else if("F".equals(value)) label = "Female";

        return label;

    }
}
