package com.kosign.firebaseauth.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.*;
import org.springframework.data.jpa.domain.support.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
@EqualsAndHashCode(callSuper=false)
public abstract class UpdatableEntity extends CreatableEntity {

    @LastModifiedDate
    private ZonedDateTime updatedOn;

}
