package com.kosign.firebaseauth.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.ZonedDateTime;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
@EqualsAndHashCode(callSuper=false)
public abstract class CreatableEntity {

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private ZonedDateTime createdOn;

}
