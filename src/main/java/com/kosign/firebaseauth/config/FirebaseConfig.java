package com.kosign.firebaseauth.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;

@Configuration
public class FirebaseConfig {

    @Value("${re.data-url}")
    private String databaseUrl;

    private static String configPath;

    @Value("${re.project-firebase}")
    public void index(String location) {
        configPath = location;
    }

    @PostConstruct
    public void init() throws IOException {

        /**
         * https://firebase.google.com/docs/server/setup
         *
         * Create service account , download json
         */

        System.out.println("********file path*********** ");
        System.out.println(configPath);
        InputStream inputStream = FirebaseConfig.class.getClassLoader().getResourceAsStream(configPath);

        GoogleCredentials credentials = GoogleCredentials.fromStream(inputStream);

        FirebaseOptions options = FirebaseOptions.builder()
                .setCredentials(credentials)
//                .setServiceAccount(inputStream)
                .setDatabaseUrl(databaseUrl).build();
        FirebaseApp.initializeApp(options);



    }
}
