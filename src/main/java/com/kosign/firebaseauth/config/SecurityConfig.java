package com.kosign.firebaseauth.config;

import com.kosign.firebaseauth.component.FirebaseAuthenticationProvider;
import com.kosign.firebaseauth.component.FirebaseFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private FirebaseFilter firebaseFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .requestMatchers()
                .antMatchers("/dashboard", "/passcode", "/appearance", "/language"
                        ,"/user/forgot-passcode", "/user/forgot-password", "/about-developer", "/login", "/meeting/create")
                .and()
                .authorizeRequests(authorize -> authorize
                        .anyRequest().authenticated()
                )
                .httpBasic(Customizer.withDefaults())
                .formLogin()
                .loginPage("/sign-in")
                .failureUrl("/sign-in?error=true")
                .permitAll()
                .and()
                .csrf()
                .disable();

        http.addFilterBefore(firebaseFilter, BasicAuthenticationFilter.class).authorizeRequests();

    }

    private String[] url={
            "/",
            "/home",
            "/css/**",
            "/js/**",
            "/image/**",
            "/v2/api-docs", "/configuration/ui", "/swagger-resources/**",
            "/configuration/security", "/swagger-ui.html", "/webjars/**", "/v2/swagger.json","/api/v1/user/register","/check-token/**"};

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(url);
    }

    @Configuration
    protected static class AuthenticationSecurity extends GlobalAuthenticationConfigurerAdapter {

        @Value("${rs.pscode.firebase.enabled}")
        private Boolean firebaseEnabled;

        @Autowired
        private FirebaseAuthenticationProvider firebaseProvider;


        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {
            System.out.println("firebaseEnable 2");
            if (firebaseEnabled) {
                auth.authenticationProvider(firebaseProvider);
            }
        }

    }

}
