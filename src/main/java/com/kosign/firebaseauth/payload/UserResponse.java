package com.kosign.firebaseauth.payload;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserResponse {
    private String id;
    private String identifier;
    private String provider;
    private String fullName;
    private String userCode;
    private String passcode;
    private String sessionId;
    private Set<GrantedAuthority> role;
    private String picture;

    @Builder
    public UserResponse(String id, String identifier, String provider, String fullName, String userCode, String passcode, String sessionId, Set<GrantedAuthority> role, String picture){
        this.fullName=fullName;
        this.id=id;
        this.identifier=identifier;
        this.provider=provider;
        this.userCode=userCode;
        this.passcode = passcode;
        this.sessionId = sessionId;
        this.role=role;
        this.picture = picture;
    }
}
