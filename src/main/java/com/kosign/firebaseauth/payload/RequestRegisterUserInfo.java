package com.kosign.firebaseauth.payload;

import lombok.*;

@Getter
@Setter
@ToString
public class RequestRegisterUserInfo {

    private String token;

    private String username;

    private String fullName;

    private String code;

    private String department;

    private String team;

    private String email;

}
