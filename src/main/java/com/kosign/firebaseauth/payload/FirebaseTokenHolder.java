package com.kosign.firebaseauth.payload;

import com.google.api.client.util.*;
import com.google.firebase.auth.*;

import java.util.*;

public class FirebaseTokenHolder {

    private FirebaseToken token;

    public FirebaseTokenHolder(FirebaseToken token) {
        this.token = token;
    }

    public String getEmail() {
        return token.getEmail();
    }

    public String getIssuer() {
        return token.getIssuer();
    }

    public String getName() {
        return token.getName();
    }

    public String getUid() {
        return token.getUid();
    }

    public String getProvider(){
        Object resp=((ArrayMap) token.getClaims().get("firebase")).get("sign_in_provider");
        String response=(String) resp;
        return response;
    }

    public String getPhoneNumber () {
        return (String) token.getClaims().get("phone_number");
    }

    public String getGoogleId() {
        String userId = ((ArrayList<String>) ((ArrayMap) ((ArrayMap) token.getClaims().get("firebase"))
                .get("identities")).get("google.com")).get(0);

        return userId;
    }

    public String getPicture(){
        return token.getPicture();
    }
}
