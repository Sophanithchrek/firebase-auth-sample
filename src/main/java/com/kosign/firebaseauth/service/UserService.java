package com.kosign.firebaseauth.service;

import com.kosign.firebaseauth.config.IAuthenticationFacade;
import com.kosign.firebaseauth.enums.ErrorCode;
import com.kosign.firebaseauth.exception.BusinessException;
import com.kosign.firebaseauth.model.user.User;
import com.kosign.firebaseauth.model.user.UserRepository;
import com.kosign.firebaseauth.model.user.UserRole;
import com.kosign.firebaseauth.payload.FirebaseTokenHolder;
import com.kosign.firebaseauth.payload.RequestRegisterUserInfo;
import com.kosign.firebaseauth.payload.UserResponse;
import com.kosign.firebaseauth.util.AuthSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    FirebaseService firebaseService;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    @Autowired
    AuthSession authSession;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private HttpServletRequest requests;

    private static String HEADER_NAME = "X-Authorization-Firebase";

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s).orElseThrow(() -> new BusinessException(ErrorCode.USER_NOT_FOUND));
        return user;
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new BusinessException(ErrorCode.USER_NOT_FOUND));
    }

    public UserResponse getUserProfile() {
        FirebaseTokenHolder response = (FirebaseTokenHolder) authenticationFacade.getAuthentication().getCredentials();
        String username = authenticationFacade.getAuthentication().getName();
        Optional<User> user = userRepository.findByUsername(username);
        return UserResponse.builder()
                .fullName(user.get().getFullName())
                .identifier(user.get().getIdentifier())
                .userCode(user.get().getUserCode())
                .provider(user.get().getProvider())
                .id(user.get().getId().toString())
                .role(user.get().getAuthorities())
                .picture(response.getPicture())
                .build();
    }

    public User findById(String userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new BusinessException(ErrorCode.USER_NOT_FOUND));
        return user;
    }

    private String getUsername(FirebaseTokenHolder token) {
        FirebaseTokenHolder tokenHolder = token;
        String username = (tokenHolder.getName() != null) ? tokenHolder.getName() : tokenHolder.getPhoneNumber();
        if(Objects.isNull(username)) username = tokenHolder.getEmail();
        return username;
    }

    public void register(RequestRegisterUserInfo info) {

        String myToken = info.getToken().isEmpty() ? authSession.getMyToken() : info.getToken();
        FirebaseTokenHolder tokenHolder = firebaseService.parseToken(myToken);
        String username = getUsername(tokenHolder);

        User user = User.builder()
                .id(tokenHolder.getUid())
                .fullName(info.getFullName())
                .identifier(tokenHolder.getProvider())
                .role(UserRole.USER)
                .userCode(info.getCode())
                .provider(tokenHolder.getProvider())
                .username(username)
                .email(info.getEmail())
                .build();
        userRepository.save(user);
    }

    public Optional<User> checkTokenToTakeUserInfo(String token) {
        System.err.println(">>>>>>> My token is: "+token);
        FirebaseTokenHolder tokenHolder = firebaseService.parseToken(token);

        if(tokenHolder!=null){
            String username = getUsername(tokenHolder);
            System.err.println(userRepository.findByUsername(username));
            return userRepository.findByUsername(username);
        }
        return null;
    }
}
