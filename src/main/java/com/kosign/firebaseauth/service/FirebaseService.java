package com.kosign.firebaseauth.service;

import com.kosign.firebaseauth.payload.FirebaseTokenHolder;
import com.kosign.firebaseauth.util.FirebaseParser;
import org.springframework.stereotype.Service;

@Service
public class FirebaseService {
    public FirebaseTokenHolder parseToken(String firebaseToken) {
        return new FirebaseParser().parseToken(firebaseToken);
    }
}
